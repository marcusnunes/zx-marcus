import formatNumber from "./formatNumber";

describe("format number", () => {
  it("should return a masked currency", () => {
    expect(formatNumber(30000)).toEqual("R$ 30.000,00");
  });

  it("should return a masked currency even if is a string", () => {
    expect(formatNumber("30000")).toEqual("R$ 30.000,00");
  });

  it("should return false if invalid number is provided", () => {
    expect(formatNumber("test")).toEqual(false);
  });
});
