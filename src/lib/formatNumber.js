export default (n) => {
  if (typeof n === "string") {
    n = Number(n);
  }

  if (isNaN(n)) return false;

  return `R$ ${n
    .toFixed(2)
    .replace(".", ",")
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}`;
};
