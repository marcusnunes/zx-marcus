import React from "react";
import { shallow, mount } from "enzyme";
import SearchBar from "./SearchBar";

const mockOnKeyDown = jest.fn();

describe("SearchBar", function() {
  it("should call the parameter function", () => {
    const wrapper = shallow(<SearchBar onKeyDown={mockOnKeyDown} />);

    wrapper.find("TextInput").simulate("ChangeText", "Skol");

    expect(mockOnKeyDown.mock.calls.length).toBe(1);
    expect(wrapper.state().value).toEqual("Skol");
  });
});
