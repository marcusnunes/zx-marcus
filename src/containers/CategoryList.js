import React, { Component } from "react";
import Categories from "../components/Categories";
import { getCategories } from "../api/categories";

export default class CategoryList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: []
    };
  }

  componentDidMount() {
    this.fetchCategories();
  }

  fetchCategories = () => {
    getCategories()
      .then(categories => this.setState({ categories }));
  };

  render() {
    return (
      <Categories
        categories={this.state.categories}
        onChange={this.props.onChange}
      />
    );
  }
}
