import React from "react";
import { shallow } from "enzyme";
import Product from "./Product";

const mockProductAdded = jest.fn();
const mockProductRemoved = jest.fn();

const newWrapper = ({ title, quantity, imageUrl, price }) => (
  <Product
    title={title}
    quantity={quantity}
    imageUrl={imageUrl}
    price={price}
    productRemoved={mockProductRemoved}
    productAdded={mockProductAdded}
  />
);

let wrapper;

describe("Product", function() {
  beforeEach(() => {
    wrapper = shallow(
      newWrapper({
        title: "Kit Budweiser",
        price: 25.9,
        quantity: 1,
        imageUrl: "image.jpg"
      })
    );
  });

  it("Should render ProductCard", () => {
    const CpProductCard = wrapper.find("ProductCard");
    expect(CpProductCard).toHaveLength(1);
    expect(CpProductCard.props().quantity).toEqual(1);
  });

  it("Should call addProduct()", () => {
    wrapper.instance().addProduct();
    wrapper.update();
    expect(wrapper.instance().state.quantity).toEqual(2);
  });

  it("Should call removeProduct()", () => {
    wrapper.instance().removeProduct();
    wrapper.instance().removeProduct();
    wrapper.update();
    expect(wrapper.instance().state.quantity).toEqual(0);
  });
});
