import React, { Component } from "react";
import ProductCard from "../components/ProductCard";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: props.quantity || 0
    };
  }

  addProduct = () => {
    const newQuantity = this.state.quantity + 1;
    this.setState({ quantity: newQuantity });
  };

  removeProduct = () => {
    if (this.state.quantity > 0) {
      const newQuantity = this.state.quantity - 1;
      this.setState({ quantity: newQuantity });
    }
  };

  render() {
    return (
      <ProductCard
        {...this.props}
				quantity={this.state.quantity}
				productAdded={this.addProduct}
        productRemoved={this.removeProduct}
      />
    );
  }
}

export default Product;
