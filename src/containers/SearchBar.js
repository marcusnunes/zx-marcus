import React, { Component } from "react";
import { TextInput, View, StyleSheet } from "react-native";

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "" || props.value
    };
  }

  onChangeText = value => {
    this.setState({ value });
    this.props.onKeyDown(value);
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          value={this.state.value}
          style={styles.searchInput}
          placeholder="Digite o produto que você procura"
          onChangeText={this.onChangeText}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignSelf: "stretch"
  },
  searchInput: {
    alignSelf: "stretch",
    padding: 10,
    backgroundColor: "#eee"
  }
});

export default SearchBar;
