import { debounce } from "lodash";
import React, { Component } from "react";
import {
  Button,
  FlatList,
  Modal,
  StyleSheet,
  Text,
  View
} from "react-native";

import Loading from "../components/Loading";
import Product from "../containers/Product";
import ProductTitle from "../components/ProductTitle";
import SearchBar from "../containers/SearchBar";
import CategoryList from "../containers/CategoryList";

import { getProducts } from "../api/products";

class ProductsList extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: <ProductTitle address={navigation.getParam("address")} />,
    headerStyle: {
      backgroundColor: "#FCD500"
    }
  });

  constructor(props) {
    super(props);

    this.state = {
      pocId: props.navigation.getParam('pocId'),
      loading: false,
      products: [],
      category: {
        id: 0,
        name: ""
      },
      search: "",
      modalVisible: false
    };
  }

  componentDidMount() {
    this.fetchProducts();
  }

  fetchProducts() {
    this.setState({ loading: true });

    getProducts(this.state.pocId, this.state.search, this.state.category.id)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  handleResponse = products => {
    this.setState({
      loading: false,
      products
    });
  };

  handleError = () => {
    this.setState({ loading: false });
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  renderItem = ({ item }) => {
    return <Product {...item} />;
  };

  render() {
    const { loading, products } = this.state;

    return (
      <View style={styles.wrapper}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
        >
          <View style={{ marginTop: 50 }}>
            <View>
              <CategoryList
                onChange={item => {
                  this.setState(
                    {
                      category: {
                        id: item.id,
                        title: item.title
                      }
                    },
                    this.fetchProducts
                  );

                  this.setModalVisible(!this.state.modalVisible);
                }}
              />
            </View>
          </View>
        </Modal>

        <Button
          title="filtrar por categoria"
          onPress={() => {
            this.setModalVisible(true);
          }}
        />

        {this.state.category.id > 0 && (
          <Text style={{ padding: 10 }}>
            mostrando somente {this.state.category.title}
          </Text>
        )}

        <SearchBar
          value={this.state.search}
          onKeyDown={debounce(search => {
            this.setState({ search }, this.fetchProducts);
          }, 500)}
        />

        {loading ? (
          <Loading />
        ) : !products.length ? (
          <Text>Nenhum produto encontrado</Text>
        ) : (
          <FlatList
            style={styles.list}
            data={products}
            renderItem={this.renderItem}
            keyExtractor={({ inventoryItemId }) => inventoryItemId}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  wrapper: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  list: {
    flex: 1,
    alignSelf: "stretch"
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: "#eee"
  }
});

export default ProductsList;
