import React, { Component } from "react";
import {
  Button,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  View
} from "react-native";

import { getLatLng } from "../api/maps";
import { getPoc } from "../api/poc";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: "",
      pocId: "",
      pocInvalid: false,
      hasError: false,
      loading: false
    };
  }

  onChange = address =>
    this.setState({
      address,
      pocInvalid: false,
      hasError: false
    });

  handleFormSubmit = () => {
    this.setState({ loading: true });

    getLatLng(this.state.address)
      .then(coords => getPoc(coords.lat, coords.lng))
      .then(this.handleResult)
      .catch(this.handleError);
  };

  handleResult = pocs => {
    if (pocs.some(poc => poc.status === "AVAILABLE")) {
      return this.setState(
        {
          loading: false,
          pocId: pocs[0].id
        },
        this.redirect
      );
    }
    this.setState({
      loading: false,
      pocInvalid: true
    });
  };

  handleError = () => {
    this.setState({
      loading: false,
      hasError: true
    });
  };

  redirect = () => {
    this.props.navigation.navigate("ProductList", {
      address: this.state.address,
      pocId: this.state.pocId
    });
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={styles.container}>
          <Text style={styles.title}>Bebida gelada, rápida a preço baixo</Text>

          {this.state.pocInvalid && (
            <Text style={styles.error}>
              Não há nenhum fornecedor disponível nesse endereço
            </Text>
          )}

          {this.state.hasError && (
            <Text style={styles.error}>Erro no serviço, tente novamente!</Text>
          )}

          <TextInput
            value={this.state.address}
            style={styles.searchInput}
            placeholder="Entre com seu endereço"
            onChangeText={this.onChange}
          />

          <Button
            title={this.state.loading ? "Buscando..." : "Buscar"}
            style={styles.button}
            onPress={this.handleFormSubmit}
            disabled={this.state.loading}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#FCD500",
    padding: 10
  },
  title: {
    fontSize: 30,
    fontWeight: "700",
    marginBottom: 20
  },
  error: {
    color: "red",
    paddingVertical: 10
  },
  searchInput: {
    fontSize: 20,
    textAlign: "left",
    alignSelf: "stretch",
    padding: 10,
    marginBottom: 20,
    backgroundColor: "#fff",
    borderColor: "#fefefe",
    borderWidth: 1
  },
  button: {
    backgroundColor: "#333333"
  }
});

export default Home;
