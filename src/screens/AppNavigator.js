import { createAppContainer, createStackNavigator } from "react-navigation";
import Home from "./Home";
import ProductList from "./ProductList";

const AppNavigator = createStackNavigator({
  Home: { screen: Home },
  ProductList: { screen: ProductList }
});

export default createAppContainer(AppNavigator);
