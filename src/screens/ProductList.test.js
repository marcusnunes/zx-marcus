import "react-native";
import React from "react";
import renderer from "react-test-renderer";
import ProductList from "./ProductList";

const createTestProps = props => ({
  navigation: {
    getParam: jest.fn()
  },
  ...props
});

it("renders correctly", () => {
  let props = createTestProps({});
  renderer.create(<ProductList {...props} />);
});
