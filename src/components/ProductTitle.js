import React from "react";
import { StyleSheet, Text, View } from "react-native";

const ProductTitle = ({ address }) => {
  return (
    <View>
      <Text style={styles.title}>Endereço de entrega</Text>
      <Text style={styles.address}>{address}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 14,
    fontWeight: "bold"
  },
  address: {
    fontSize: 12
  }
});

export default ProductTitle;
