import React from "react";
import { Button, Image, StyleSheet, Text, View } from "react-native";
import PropTypes from "prop-types";

const ProductCard = ({
  title,
  quantity,
  imageUrl,
  price,
  productRemoved,
  productAdded
}) => {
  return (
    <View>
      <View style={[styles.item, styles.border]}>
        <Image style={styles.image} source={{ uri: imageUrl }} />
        <View style={styles.rightContent}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.price}>R$ {price}</Text>

          <View style={styles.item}>
            <Button
              onPress={() => productRemoved(quantity)}
              title="-"
              color="red"
            />
            <Text>{quantity}</Text>
            <Button
              onPress={() => productAdded(quantity)}
              title="+"
              color="green"
            />

            <Button
              title="adicionar"
              color="green"
              onPress={() => console.log("todo: add to cart")}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 10,
    flexDirection: "row",
    alignItems: "center"
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: "#e0e3e7"
  },
  rightContent: {
    flex: 1,
    marginLeft: 10
  },
  image: {
    width: 150,
    height: 150,
    backgroundColor: "gray"
  },
  title: {
    fontSize: 14
  },
  price: {
    fontSize: 16,
    fontWeight: "bold",
    paddingTop: 10
  }
});

ProductCard.propTypes = {
  title: PropTypes.string.isRequired,
  quantity: PropTypes.number,
  imageUrl: PropTypes.string.isRequired,
  price: PropTypes.number,
  productRemoved: PropTypes.func.isRequired,
  productAdded: PropTypes.func.isRequired
};

ProductCard.defaultProps = {
  quantity: 0,
  price: 0
};

export default ProductCard;
