import React from "react";
import { shallow } from "enzyme";
import ProductCard from "./ProductCard";

const mockProductAdded = jest.fn();
const mockProductRemoved = jest.fn();

const newWrapper = ({ title, quantity, imageUrl, price }) => (
  <ProductCard
    title={title}
    quantity={quantity}
    imageUrl={imageUrl}
    price={price}
    productRemoved={mockProductRemoved}
    productAdded={mockProductAdded}
  />
);

let wrapper;

describe("ProductCard", function() {
  beforeEach(() => {
    wrapper = shallow(
      newWrapper({
        title: "Kit Budweiser",
        price: 25.9,
        quantity: 0,
        imageUrl: "image.jpg"
      })
    );
  });

  afterEach(() => {
    mockProductAdded.mockClear();
    mockProductRemoved.mockClear();
  });

  it("Should call productAdded()", () => {
    const CpButton = wrapper.find("Button").at(1);
    CpButton.simulate("press");

    expect(mockProductAdded.mock.calls.length).toBe(1);
    expect(mockProductRemoved.mock.calls.length).toBe(0);
  });

  it("Should call productRemoved()", () => {
    const CpButton = wrapper.find("Button").at(0);
    CpButton.simulate("press");

    expect(mockProductAdded.mock.calls.length).toBe(0);
    expect(mockProductRemoved.mock.calls.length).toBe(1);
  });
});
