import React from "react";
import { FlatList, Text, View, TouchableOpacity } from "react-native";

export default props => {
  const initial = {
    id: "0",
    title: "Mostrar todos"
  };

  const categories = [ initial, ...props.categories ];

  return (
    <View>
      <Text>Selecione a categoria</Text>
      <FlatList
        data={categories}
        keyExtractor={({ id }) => id}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => props.onChange(item)}>
            <Text key={item.id}>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
