import React from "react";
import { shallow } from "enzyme";
import Categories from "./Categories";

const fakeOnChange = jest.fn();

const fakeCategories = [
  { id: "1", title: "Bebidas" },
  { id: "2", title: "Kits" }
];

const fakeCategoriesFinal = [
  { id: "0", title: "Mostrar todos" },
  ...fakeCategories
];

const newWrapper = ({ categories, onChange }) => (
  <Categories categories={categories} onChange={onChange} />
);

let wrapper;

describe("Categories", function() {
  beforeEach(() => {
    wrapper = shallow(
      newWrapper({
        categories: fakeCategories,
        onChange: fakeOnChange
      })
    );
  });

  it("should render FlatList", () => {
    const CpFlatList = wrapper.find("FlatList");
    expect(CpFlatList).toHaveLength(1);
    expect(CpFlatList.props().data).toEqual(fakeCategoriesFinal);
    expect(typeof CpFlatList.props().keyExtractor).toBe("function");
    expect(typeof CpFlatList.props().renderItem).toBe("function");
  });
});
