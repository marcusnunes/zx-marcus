const apiKey = 'AIzaSyB2nGmcSh8uon2DwOsehUzrhdpVH5snXe8';

export const getLatLng = address => {
  const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURI(address)}&key=${apiKey}`;

  return fetch(url)
    .then(response => response.json())
    .then(responseJson =>
      Promise.resolve(responseJson.results[0].geometry.location)
    )
    .catch(error => Promise.reject(error));
};
