import client from "./client";

const query = `query pocCategorySearch($id: ID!, $search: String!, $categoryId: Int!) {
    poc(id: $id) {
      products(categoryId: $categoryId, search: $search) {
        productVariants{
          inventoryItemId,
          title
          imageUrl
          price
        }
      }
    }
}`;

export const getProducts = (id, search = "", categoryId = 0) => {
  return client({
    query,
    variables: {
      id,
      search,
      categoryId
    }
  })
    .then(({ data }) =>
      Promise.resolve(
        data.poc.products.map(product => product.productVariants[0])
      )
    )
    .catch(error => Promise.reject(error));
};
