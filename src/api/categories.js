import client from "./client";

export const getCategories = () => {
  const query = `query allCategoriesSearch {
      allCategory {
        id, 
        title
      }
    }`;

  return client({ query })
    .then(({ data }) => Promise.resolve(data.allCategory))
    .catch(error => Promise.reject(error));
};
