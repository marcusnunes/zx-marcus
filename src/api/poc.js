import client from "./client";

const query = `query pocSearchMethod($now: DateTime!, $algorithm: String!, $lat: String!, $long: String!) {
    pocSearch(now: $now, algorithm: $algorithm, lat: $lat, long: $long) {
      id
      status
    }
  }`;

const handleResponse = response => {
  if (response.errors) {
    return Promise.reject(response.errors.join());
  }
  return response.data.pocSearch;
};

export const getPoc = (lat, long) => {
  return client({
    query,
    variables: {
      now: new Date(),
      algorithm: "NEAREST",
      lat,
      long
    }
  })
    .then(({ data }) => Promise.resolve(data.pocSearch))
    .catch(error => Promise.reject(error));
};
