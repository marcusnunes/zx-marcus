import React, { Component } from "react";
import AppNavigator from "./src/screens/AppNavigator";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: []
    };
  }

  render() {
    return (
      <AppNavigator
        screenProps={{
          cart: this.state.cart
        }}
      />
    );
  }
}
