## Installation

In the project directory, you can run:

1. `npm install`

2. `npm start`

You will need expo cli to start the app.

## Unit tests

1. `npm test`
